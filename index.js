const express = require("express");
const { setupLogging } = require("./logging");
const { ROUTES } = require("./routes");
const { setupProxies } = require("./proxy");
require("dotenv").config();

const app = express();

setupLogging(app);
setupProxies(app, ROUTES);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.get("/", (req, res) => res.send("Hello World!"));
app.listen(4000, () => console.log("API Gateway server started"));
