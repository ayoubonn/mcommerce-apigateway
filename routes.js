const ROUTES = [
  {
    url: "/api/Products",
    auth: false,
    creditCheck: false,
    rateLimit: {
      windowMs: 15 * 60 * 1000,
      max: 5,
    },
    proxy: {
      target: "http://localhost:3001",
      changeOrigin: true,
      pathRewrite: {
        [`^/api/Products`]: "/api/Products",
      },
    },
  },
  {
    url: "/api/commandes",
    auth: false,
    creditCheck: false,
    rateLimit: {
      windowMs: 15 * 60 * 1000,
      max: 5,
    },
    proxy: {
      target: "http://localhost:3002",
      changeOrigin: true,
      pathRewrite: {
        [`^/api/commandes`]: "/api/commandes",
      },
    },
  },
  {
    url: "/api/payments",
    auth: false,
    creditCheck: false,
    rateLimit: {
      windowMs: 15 * 60 * 1000,
      max: 5,
    },
    proxy: {
      target: "http://localhost:3003",
      changeOrigin: true,
      pathRewrite: {
        [`^/api/payments`]: "/api/payments",
      },
    },
  },
];

exports.ROUTES = ROUTES;
